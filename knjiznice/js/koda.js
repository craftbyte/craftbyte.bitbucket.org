
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var imenaM = ["Franc","Janez","Ivan","Anton","Marko","Andrej","Jožef","Jože","Luka","Peter","Marjan","Matej","Tomaž","Milan","Aleš","Branko","Bojan","Robert","Rok","Boštjan","Matjaž","Gregor","Miha","Stanislav","Martin","David","Igor","Jan","Dejan","Boris","Dušan","Nejc","Žiga","Jure","Alojz","Uroš","Blaž","Mitja","Žan","Simon","Matic","Klemen","Darko","Primož","Jernej","Drago","Gašper","Aleksander","Anže","Jaka","Aljaž","Miran","Tadej","Denis","Jakob","Roman","Štefan","Nik","Vladimir","Damjan","Srečko","Borut","Matija","Slavko","Janko","Tilen","Mirko","Zoran","Filip","Miroslav","Alen","Domen","Danijel","Vid","Stanko","Goran","Tim","Mark","Mihael","Matevž","Leon","Urban","Sašo","Jurij","Andraž","Iztok","Marijan","Vinko","Alojzij","Viktor","Dragan","Maks","Benjamin","Rudolf","Zvonko","Erik","Samo","Zdravko","Lovro","Danilo","Gal","Gorazd","Rajko","Pavel","Zlatko","Edvard","Kristjan","Bogdan","Sandi","Patrik","Sebastjan","Maj","Anej","Franci","Emil","Timotej","Vojko","Željko","Silvo","Lan","Josip","Damir","Ludvik","Aljoša","Dominik","Damijan","Ciril","Nikola","Miloš","Albin","Daniel","Niko","Božidar","Tine","Aleks","Aleksandar","Saša","Tomislav","Silvester","Stojan","Leopold","Viljem","Davorin","Mario","Frančišek","Grega","Valentin","Kristijan","Mladen","Oskar","Marcel","Karel","Davor","Vlado","Vincenc","Rene","Franjo","Zdenko","Bor","Enej","Jasmin","Tian","Nenad","Ladislav","Sebastijan","Elvis","Bogomir","Ivo","Edin","Jani","Val","Valter","Ervin","Karl","Rado","Teo","Ernest","Stjepan","Nino","Maksimiljan","Sergej","Izidor","Petar","Nikolaj","Senad","Vito","Mirsad","Metod","Alex","Slobodan","Liam","Renato","Samir","Avgust","Jaša","Radovan","Lenart","Svit","Nace","Dalibor"];
var imenaZ = ["Marija","Ana","Maja","Irena","Mojca","Mateja","Nina","Nataša","Andreja","Barbara","Jožica","Petra","Eva","Anja","Katja","Sara","Jožefa","Sonja","Tatjana","Katarina","Tanja","Milena","Tina","Alenka","Vesna","Martina","Majda","Urška","Nika","Ivana","Špela","Frančiška","Tjaša","Anica","Helena","Terezija","Dragica","Nada","Darja","Kristina","Simona","Danica","Olga","Marjeta","Zdenka","Suzana","Lara","Neža","Lidija","Antonija","Ema","Angela","Sabina","Vida","Marta","Janja","Ivanka","Maša","Silva","Veronika","Karmen","Darinka","Zala","Ljudmila","Aleksandra","Anita","Alojzija","Lana","Brigita","Kaja","Klara","Metka","Jana","Stanislava","Lucija","Monika","Lea","Cvetka","Natalija","Štefanija","Nevenka","Elizabeta","Jasmina","Marjana","Renata","Branka","Slavica","Tamara","Saša","Julija","Hana","Manca","Klavdija","Bernarda","Vera","Bojana","Erika","Danijela","Pavla","Teja","Ajda","Jasna","Romana","Mira","Polona","Laura","Valentina","Jelka","Mirjana","Sandra","Rozalija","Sanja","Valerija","Tadeja","Maruša","Nuša","Živa","Ines","Patricija","Mihaela","Breda","Neja","Zoja","Ksenija","Ida","Pia","Karolina","Vanja","Gabrijela","Viktorija","Mia","Ela","Marina","Vlasta","Alja","Marjetka","Melita","Taja","Albina","Julijana","Magdalena","Brina","Gordana","Ljubica","Marinka","Nadja","Karin","Matilda","Tea","Polonca","Urša","Tinkara","Marica","Amalija","Nastja","Vita","Zofija","Emilija","Cecilija","Larisa","Damjana","Gaja","Lina","Tia","Doroteja","Milka","Iva","Žana","Milica","Jerneja","Marijana","Jelena","Nives","Rebeka","Justina","Štefka","Dušanka","Andrejka","Stanka","Slavka","Marjanca","Mirjam","Lilijana","Ula","Irma","Ana Marija","Zlatka","Iris","Daša","Iza","Zvonka","Miroslava","Jolanda","Tara","Meta","Ivica","Blanka","Zarja","Naja","Anamarija"];
var priimki = ["Novak","Horvat","Kovačič","Krajnc","Zupančič","Potočnik","Kovač","Mlakar","Kos","Vidmar","Golob","Turk","Kralj","Božič","Korošec","Bizjak","Zupan","Hribar","Kotnik","Kavčič","Rozman","Kastelic","Oblak","Petek","Hočevar","Žagar","Kolar","Košir","Koren","Klemenčič","Zajc","Knez","Medved","Petrič","Zupanc","Pirc","Hrovat","Pavlič","Kuhar","Lah","Zorko","Uršič","Tomažič","Babič","Erjavec","Sever","Jereb","Jerman","Majcen","Kranjc","Rupnik","Pušnik","Breznik","Lesjak","Kovačević","Perko","Močnik","Pečnik","Furlan","Dolenc","Vidic","Pavlin","Logar","Jenko","Ribič","Žnidaršič","Tomšič","Janežič","Jelen","Petrović","Marolt","Blatnik","Pintar","Maček","Dolinar","Černe","Gregorič","Mihelič","Lešnik","Cerar","Kokalj","Zadravec","Hren","Bezjak","Fras","Leban","Čeh","Jug","Rus","Kocjančič","Vidovič","Kobal","Bogataj","Primožič","Kolenc","Hodžić","Lavrič","Kolarič","Lazar","Jovanović","Mrak","Kodrič","Nemec","Kosi","Debeljak","Marković","Žižek","Žibert","Tavčar","Ivančič","Miklavčič","Krivec","Jarc","Vodopivec","Vovk","Hribernik","Likar","Toplak","Ilić","Kramberger","Zver","Gorenc","Leskovar","Skok","Železnik","Jazbec","Stopar","Sitar","Meglič","Eržen","Demšar","Simonič","Šinkovec","Blažič","Petrovič","Nikolić","Popović","Ramšak","Javornik","Jamnik","Hozjan","Kočevar","Filipič","Bregar","Čuk","Gorjup","Podgoršek","Kramar","Volk","Koželj","Sušnik","Savić","Bukovec","Pintarič","Kokol","Rajh","Mohorič","Pogačnik","Godec","Resnik","Rutar","Gajšek","Gashi","Rožman","Šmid","Hafner","Lebar","Mavrič","Rožič","Bergant","Povše","Kumer","Gomboc","Krasniqi","Mlinar","Zemljič","Pavlović","Ambrožič","Bajc","Begić","Kristan","Bevc","Cvetko","Zakrajšek","Tratnik","Zorman","Babić","Kalan","Mlinarič","Hadžić","Markovič","Pogačar","Trček","Jerič","Kaučič","Založnik","Gorišek","Humar","Zalokar","Zalar"];

var patient;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
async function generirajPodatke() {
  var gender, name, surname, dob;
  if (Math.random() >= 0.5) {
    gender = "MALE";
    name = randItem(imenaM);
  } else {
    gender = "FEMALE";
    name = randItem(imenaZ);
  }
  surname = randItem(priimki);
  var realDob = randDateBetween(new Date(1930,1), new Date());
  dob = realDob.toISOString().substring(0, 10);
  var id = await makePatient({
    firstNames: name,
    lastNames: surname,
    gender: gender,
    dateOfBirth: dob
  })
  for (var i = 0; i < 10; i++) {
    var body = {
      "ctx/language": "en",
      "ctx/territory": "SI",
      "ctx/time": (randDateBetween(realDob, new Date())).toISOString(),
      "vital_signs/height_length/any_event/body_height_length": 120+(Math.floor(Math.random()*100)*80/100),
      "vital_signs/body_weight/any_event/body_weight": 60+(Math.floor(Math.random()*100)*60/100),
      "vital_signs/body_temperature/any_event/temperature|magnitude": 32+(Math.floor(Math.random()*100)*10/100),
      "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      "vital_signs/pulse/any_event/rate|magnitude": 50+(Math.floor(Math.random()*100)*150/100),
      "vital_signs/pulse/any_event/rate|unit": "/min",
      "vital_signs/blood_pressure/any_event/systolic": 50+(Math.floor(Math.random()*100)*100/100),
      "vital_signs/blood_pressure/any_event/diastolic": 70+(Math.floor(Math.random()*100)*100/100),
      "vital_signs/indirect_oximetry:0/spo2|numerator": 90+(Math.floor(Math.random()*100)*10/100)
    }
    var url = new URL(baseUrl+"/composition");
    url.search = new URLSearchParams({
      ehrId: id,
      templateId: 'Vital Signs',
      format: 'FLAT',
      committer: "dr. "+randItem(imenaM.concat(imenaZ))+" "+randItem(priimki)
    })
    var res = await fetch(url, {
      headers: new Headers({
        "Content-Type": "application/json",
        "Authorization": getAuthorization()
      }),
      method: "POST",
      body: JSON.stringify(body)
    }).then(res => res.json())
  }
  return id;
}

async function generirajPodatkeInPolni() {
  var ids = [];
  var id = await generirajPodatke();
  ids.push(id)
  id = await generirajPodatke();
  ids.push(id)
  id = await generirajPodatke();
  ids.push(id)
  $("#ehrid-nav").val(id)
  getPatient();
  alert("EHR IDji so: " + ids.join(", "))
}

async function narediIzPodatkov() {
  var gender, name, surname, dob;
  name = $("#newName").val();
  surname = $("#newSurname").val();
  gender = $("#newFemale").is(':checked') ? "FEMALE" : "MALE";
  dob = $("#newDob").val();
  var id = await makePatient({
    firstNames: name,
    lastNames: surname,
    gender: gender,
    dateOfBirth: dob
  })
  $("#ehrid-nav").val(id)
  getPatient();
}

async function getPatient() {
  var ehrId = $("#ehrid-nav").val();
  patient = await fetch(baseUrl+'/demographics/ehr/'+ehrId+'/party', {
    headers: new Headers({
      "Authorization": getAuthorization()
    })
  }).then(res => res.json());
  patient = patient.party
  $("#currentId").html(patient.additionalInfo.ehrId);
  $("#currentName").html(patient.firstNames || "Ni definiran");
  $("#currentSurname").html(patient.lastNames || "Ni definiran");
  $("#currentDob").html(patient.dateOfBirth || "Ni definiran");
  $("#currentGender").html(patient.gender || "Ni definiran");
  var latest = await getLatestMeasurements();
  ['blood_pressure', 'body_temperature', 'height', 'pulse', 'spO2', 'weight']
  $("#currentHeight").html(latest.height.height + latest.height.unit);
  $("#lastHeight").html((new Date(latest.height.time).toISOString().substring(0,10)));
  $("#currentWeight").html(latest.weight.weight + latest.weight.unit);
  $("#lastWeight").html((new Date(latest.weight.time).toISOString().substring(0,10)));
  $("#currentTemp").html(latest.body_temperature.temperature + latest.body_temperature.unit);
  $("#lastTemp").html((new Date(latest.body_temperature.time).toISOString().substring(0,10)));
  $("#currentSis").html(latest.blood_pressure.systolic + latest.blood_pressure.unit);
  $("#lastSis").html((new Date(latest.blood_pressure.time).toISOString().substring(0,10)));
  $("#currentDia").html(latest.blood_pressure.diastolic + latest.blood_pressure.unit);
  $("#lastDia").html((new Date(latest.blood_pressure.time).toISOString().substring(0,10)));
  $("#currentSpO2").html(latest.spO2.spO2 + "%");
  $("#lastSpO2").html((new Date(latest.spO2.time).toISOString().substring(0,10)));
  $("#currentPulse").html(latest.pulse.pulse + latest.pulse.unit);
  $("#lastPulse").html((new Date(latest.pulse.time).toISOString().substring(0,10)));
  $("#currentBMI").html(Math.round(latest.weight.weight/(latest.height.height*latest.height.height)*100000)/10);
  return patient
}

async function makePatient(newPatient) {
  if (!newPatient.hasOwnProperty("additionalInfo")) newPatient.additionalInfo = {};
  var req1 = await fetch(baseUrl+"/ehr", {
    method: "POST",
    headers: new Headers({
      "Authorization": getAuthorization()
    }),
    cache: 'no-cache'
  }).then(res => res.json())
  if (req1.action !== "CREATE") return "";
  newPatient.additionalInfo.ehrId = req1.ehrId;
  var req2 = await fetch(baseUrl+"/demographics/party", {
    body: JSON.stringify(newPatient),
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json",
      "Authorization": getAuthorization()
    }),
    cache: 'no-cache'
  }).then(res => res.json())
  if (req2.action !== "CREATE") return "";
  return newPatient.additionalInfo.ehrId;
}

async function dodajMeritve() {
  var data = {
    height: $("#dodajVitalnoTelesnaVisina").val(),
    weight: $("#dodajVitalnoTelesnaTeza").val(),
    temperature: $("#dodajVitalnoTelesnaTemperatura").val(),
    sistolic: $("#dodajVitalnoKrvniTlakSistolicni").val(),
    diastolic: $("#dodajVitalnoKrvniTlakDiastolicni").val(),
    spO2: $("#dodajVitalnoNasicenostKrviSKisikom").val(),
    pulse: $("#dodajVitalnoPulz").val(),
    commiter: $("#dodajVitalnoMerilec").val(),
  };
  var body = {
    "ctx/language": "en",
    "ctx/territory": "SI",
    "ctx/time": (new Date()).toISOString(),
    "vital_signs/height_length/any_event/body_height_length": data.height,
    "vital_signs/body_weight/any_event/body_weight": data.weight,
    "vital_signs/body_temperature/any_event/temperature|magnitude": data.temperature,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/pulse/any_event/rate|magnitude": data.pulse,
    "vital_signs/pulse/any_event/rate|unit": "/min",
    "vital_signs/blood_pressure/any_event/systolic": data.sistolic,
    "vital_signs/blood_pressure/any_event/diastolic": data.diastolic,
    "vital_signs/indirect_oximetry:0/spo2|numerator": data.spO2
  }
  var url = new URL(baseUrl+"/composition");
  url.search = new URLSearchParams({
    ehrId: patient.additionalInfo.ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: data.commiter
  })
  var res = await fetch(url, {
    headers: new Headers({
      "Content-Type": "application/json",
      "Authorization": getAuthorization()
    }),
    method: "POST",
    body: JSON.stringify(body)
  }).then(res => res.json())
  if (res.action === "CREATE") return true;
  return false;
}

async function getMeasurement(name) {
  var id = patient.additionalInfo.ehrId;
  return fetch(baseUrl+'/view/'+id+'/'+name, {
    headers: new Headers({
      'Authorization': getAuthorization()
    })
  }).then(res => res.json()).then(res => {
    return {
      name: name, val: res
    }
  })
}

async function getLatestMeasurements() {
  var promises = [];
  var links = ['blood_pressure', 'body_temperature', 'height', 'pulse', 'spO2', 'weight']
  for (let link of links) {
    promises.push(getMeasurement(link))
  }
  var result = {}
  await Promise.all(promises).then(vals => {
    for (val of vals) {
      result[val.name] = val.val[0];
    }
  })
  return result;
}

/**
 * Izbere naključni element iz seznama, uporabni pri generiranju naključnih podatkov.
 * @param {array} list seznam
 * @returns naključni element iz `list`
 */
function randItem(list) {
  if (typeof list !== "object") return false;
  return list[Math.floor(Math.random()*list.length)];
}

function randDateBetween(from, to) {
  from = from.getTime();
  to = to.getTime();
  return new Date(from + Math.random() * (to - from));
}

async function getNews() {
  var feed = await fetch('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.medicaldaily.com%2Frss').then(res => res.json())
  $("#news").html("");
  for (let item of feed.items) {
    $("#news").append('<li><a href='+item.link+'">'+item.title+'</a></li>')
  }
}